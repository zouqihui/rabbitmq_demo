<?php
/**
 * Created by PhpStorm.
 * User: huyi
 * Date: 2018/1/16
 * Time: 17:16
 */
require_once './Queue.php';

class Client extends Queue{
    public function __construct(){
        parent::__construct();
    }

    //接受消息
    public function recMessage(){
        while(true){
                $this -> _queueObject -> consume(function (AMQPEnvelope $e,AMQPQueue $q){
                $requestUrl = $e -> getBody();
                if($requestUrl){
                    $q->nack($e->getDeliveryTag());
                }else{
                    usleep(100);
                }
            });
        }
    }
}