<?php
/**
 * Created by PhpStorm.
 * User: huyi
 * Date: 2018/1/16
 * Time: 11:07
 */
$exchangeName = 'demo';
$queueName    = 'task_queue';
$routeKey     = 'task_route';

$config = array(
    'host'     => '127.0.0.1',
    'port'     => 5672,
    'vhost'    => '/',
    'login'    => 'huyi',
    'password' => 'huyi123'
);

$conn = new AMQPConnection($config);

$conn->connect() or die("can not connect to the broken!");

$channel = new AMQPChannel($conn);//channel
$exchange = new AMQPExchange($channel);
$exchange -> setName($exchangeName);
$exchange -> setType(AMQP_EX_TYPE_DIRECT);
$exchange->declareExchange();

$queue = new AMQPQueue($channel);
$queue -> setName($queueName);
$queue -> setFlags(AMQP_DURABLE);
$queue -> declareQueue();

$queue -> bind($exchangeName,$routeKey);

while(true){
    $queue -> consume('callback');
    $channel -> qos(0,1);
}
$conn -> disconnect();






function callback(AMQPEnvelope $envelope,AMQPQueue $queue) {
    $msg = $envelope->getBody();
    var_dump(" [x] Received:" . $msg);
    sleep(substr_count($msg,'.'));
    $queue->ack($envelope->getDeliveryTag());
}