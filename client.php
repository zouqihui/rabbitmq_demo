<?php
/**
 * Created by PhpStorm.
 * User: huyi
 * Date: 2018/1/15
 * 生产者：发送消息
 * 逻辑：
 * 创建连接-->创建channel-->创建交换机对象-->发送消息 Continue reading →
 */

$config = array(
    'host'     => '127.0.0.1',
    'port'     => '5672',
    'login'    => 'huyi',
    'password' => 'huyi123',
    'vhost'    => '/'
);

$e_name  = 'e_huyi'; //交换机名
$q_name  = 'q_huyi'; //队列名
$k_route = 'key_1'; //路由key

$conn = new AMQPConnection();
$conn->connect();
if(!$conn->connect()){
    die("Cannot connect to the broker!\n");
}

$channelObj  = new AMQPChannel($conn);
$exchangeObj = new AMQPExchange($channelObj);
$exchangeObj->setName($e_name);
$exchangeObj->setType(AMQP_EX_TYPE_DIRECT);
$messageBody = 'welcome to rabbitmq';
$exchangeObj->publish($messageBody, $k_route);
$conn->disconnect();