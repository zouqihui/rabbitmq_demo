<?php

require_once('rabbitmq.php');

//一般模式
//$rabbitmq = new rabbitmq();
//$rabbitmq->commonConsume('common', 'commonBack');

function commonBack($msg)
{
    echo " 放大放大三 ", $msg->body, "\n";
}

//工作模式

//$rabbitmq_work = new rabbitmq();
//$rabbitmq_work->workConsume('work' ,'workBack');
function workBack($msg)
{
    echo " [x] Received ", $msg->body, "\n";
    sleep(substr_count($msg->body, '.'));
    echo " [x] Done", "\n";
    echo " 放大放大三 ", $msg->body, "\n";
    $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
}

//发布订阅
//$rabbitmq_pubsub = new rabbitmq();
//$rabbitmq_pubsub->pubsubConsume('pubsub','pubsubBack');


function pubsubBack($msg)
{
    echo " 放大放大三 ", $msg->body, "\n";
    echo ' [x] ', $msg->body, "\n";
}


//路由模式
//$rabbitmq_rout = new rabbitmq();
//$rabbitmq_rout->routConsume('rout',['or','blank'], 'routBack');

function routBack($msg)
{
    echo ' [x] ',$msg->delivery_info['routing_key'], ':', $msg->body, "\n";
}

//主题模式
$rabbitmq_topic = new rabbitmq();
$rabbitmq_topic->topicConsume('topic_c',['warn','*'],'topicBack');

function topicBack($msg)
{
    echo ' [x] ',$msg->delivery_info['routing_key'], ':', $msg->body, "\n";
}

//自定义模式

$rabbitmq_topic = new rabbitmq();
//$rabbitmq_topic->cusComConsume('topic_c',['warn','*'],'topicComBack');
$rabbitmq_topic->cusWorkConsume('topic_c',['warn','*'],'topicComBack','cuswork');

function topicComBack($msg)
{
    echo ' [x] ',$msg->delivery_info['routing_key'], ':', $msg->body, "\n";
}

