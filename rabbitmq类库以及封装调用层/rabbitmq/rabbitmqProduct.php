<?php
require_once('rabbitmq.php');

//一般模式
//$rabbitmq = new rabbitmq();
//$rabbitmq->commonProduct('common', 'zhe shi yig3332323e common');

//工作模式
//$rabbitmq_work = new rabbitmq();
//$rabbitmq_work->workProduct('work', 'zhe shi work 1');

//发布订阅模式
//$rabbitmq_pubsub = new rabbitmq();
//$rabbitmq_pubsub->pubsubProduct('pubsub', 'fabu ding yu le ');

//路由模式
//$rabbitmq_rout = new rabbitmq();
//$rabbitmq_rout->routProduct('rout', 'rout le l33ele','blank');

//$rabbitmq_topic = new rabbitmq();
//$rabbitmq_topic->topicProduct('topic_c','warn','aa2323232');

//自定义模式
$rabbitmq_cus = new rabbitmq();
$rabbitmq_cus->cusProduct('topic_c','kern.critical','A critical kernel error');

